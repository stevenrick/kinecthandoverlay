'''
Created on August 5, 2014

@author: rick

xSkeleton = ((xNorm - 0.5) * depth * multiplier)
xSkeleton is the meter value
xNorm is a "normalized" x coordinate (left edge of image is 0.0, right edge of image is 1.0)
can take xNorm and multiply by resolution for pixel value
kinectXResolution = 640
kinectYResolution = 480
multiplier is a constant:
in the X dimension is 1.12032
in the Y dimension is 0.84024
depth is z value

left elbow x,y,z = 16,17,18
left wrist x,y,z = 19,20,21
left hand x,y,z = 22,23,24
right elbow x,y,z = 28,29,30
right wrist x,y,z = 31,32,33
right hand x,y,z = 34,35,36

'''

import csv
import os
import Tkinter, tkFileDialog
import tkMessageBox
from PIL import Image, ImageDraw, ImageTk #seeing an error? you need pip, run get-pip.py then install PIL from command line with 'pip install pillow'
import fnmatch
from random import shuffle


def hms_to_seconds(tIn):
    tCells = tIn.split(':')
    if len(tCells) == 3:
        #has h:m:s
        return int(tCells[0]) * 3600 + int(tCells[1]) * 60 + float(tCells[2])
    else:
        if len(tCells) == 2:
            #has m:s
            return int(tCells[0]) * 60 + float(tCells[1])
        else:
            #has s
            return float(tCells[0])


def convert2Pixel(filePath):
    print 'Converting bodyTracking meters to pixels'
    kinectXResolution = 640
    kinectYResolution = 480
    inputCsv = filePath
    pixelFilePath = os.path.splitext(inputCsv)[0]+"_pixels.csv"
    skipHeader = True
    working_output = []
    
    leftBodyTrackingDict = {}
    rightBodyTrackingDict = {}
    
    #print "Converting meters to pixels"
    with open(inputCsv, 'rU') as csvIn, open(pixelFilePath,'wb') as meterOut:
        csvData = csv.reader(csvIn)
        outWriter = csv.writer(meterOut, delimiter=',')
        header = 'Time,ElbowLeftX,ElbowLeftY,WristLeftX,WristLeftY,HandLeftX,HandLeftY,ElbowRightX,ElbowRightY,WristRightX,WristRightY,HandRightX,HandRightY'
        outWriter.writerow(header.split(','))
        for row in csvData:
            if skipHeader:
                skipHeader = False
            else:
                try:
                    if row[16] != '' and row[17] != '' and row[18] != '':
                        leftElbowX = float(row[16])
                        leftElbowY = float(row[17])
                        leftElbowZ = float(row[18])
                        leftElbowXPx = kinectXResolution*(((leftElbowX)/(leftElbowZ*1.12032))+0.5)
                        leftElbowYPx = kinectYResolution*(((leftElbowY)/(leftElbowZ*0.84024))+0.5)
                    else:
                        leftElbowX = ''
                        leftElbowY = ''
                        leftElbowZ = ''
                        leftElbowXPx = ''
                        leftElbowYPx = ''
                        
                    if row[19] != '' and row[20] != '' and row[21] != '':
                        leftWristX = float(row[19])
                        leftWristY = float(row[20])
                        leftWristZ = float(row[21])
                        leftWristXPx = kinectXResolution*(((leftWristX)/(leftWristZ*1.12032))+0.5)
                        leftWristYPx = kinectYResolution*(((leftWristY)/(leftWristZ*0.84024))+0.5)
                    else:
                        leftWristX = ''
                        leftWristY = ''
                        leftWristZ = ''
                        leftWristXPx = ''
                        leftWristYPx = ''
                        
                    if row[22] != '' and row[23] != '' and row[24] != '':
                        leftHandX = float(row[22])
                        leftHandY = float(row[23])
                        leftHandZ = float(row[24])
                        leftHandXPx = kinectXResolution*(((leftHandX)/(leftHandZ*1.12032))+0.5)
                        leftHandYPx = kinectYResolution*(((leftHandY)/(leftHandZ*0.84024))+0.5)
                    else:
                        leftHandX = ''
                        leftHandY = ''
                        leftHandZ = ''
                        leftHandXPx = ''
                        leftHandYPx = ''
                        
                    if row[28] != '' and row[29] != '' and row[30] != '':
                        rightElbowX = float(row[28])
                        rightElbowY = float(row[29])
                        rightElbowZ = float(row[30])
                        rightElbowXPx = kinectXResolution*(((rightElbowX)/(rightElbowZ*1.12032))+0.5)
                        rightElbowYPx = kinectYResolution*(((rightElbowY)/(rightElbowZ*0.84024))+0.5)
                    else:
                        rightElbowX = ''
                        rightElbowY = ''
                        rightElbowZ = ''
                        rightElbowXPx = ''
                        rightElbowYPx = ''
                        
                    if row[31] != '' and row[32] != '' and row[33] != '':
                        rightWristX = float(row[31])
                        rightWristY = float(row[32])
                        rightWristZ = float(row[33])
                        rightWristXPx = kinectXResolution*(((rightWristX)/(rightWristZ*1.12032))+0.5)
                        rightWristYPx = kinectYResolution*(((rightWristY)/(rightWristZ*0.84024))+0.5)
                    else:
                        rightWristX = ''
                        rightWristY = ''
                        rightWristZ = ''
                        rightWristXPx = ''
                        rightWristYPx = ''
                    
                    if row[34] != '' and row[35] != '' and row[36] != '':
                        rightHandX = float(row[34])
                        rightHandY = float(row[35])
                        rightHandZ = float(row[36])
                        rightHandXPx = kinectXResolution*(((rightHandX)/(rightHandZ*1.12032))+0.5)
                        rightHandYPx = kinectYResolution*(((rightHandY)/(rightHandZ*0.84024))+0.5)
                    else:
                        rightHandX = ''
                        rightHandY = ''
                        rightHandZ = ''
                        rightHandXPx = ''
                        rightHandYPx = ''
                        
                    
                    working_output.append(
                                          [
                                           str(row[0]),
                                           str(leftElbowXPx),str(leftElbowYPx),
                                           str(leftWristXPx),str(leftWristYPx),
                                           str(leftHandXPx),str(leftHandYPx),
                                           str(rightElbowXPx),str(rightElbowYPx),
                                           str(rightWristXPx),str(rightWristYPx),
                                           str(rightHandXPx),str(rightHandYPx)
                                           ]
                                          )
                    for item in working_output:
                        writeHolder = ','.join(item)
                    outWriter.writerow(writeHolder.split(','))
                    
                    time = (hms_to_seconds(row[0]))
                    #print time
                    leftBodyTrackingDict[time] = [leftElbowX, leftElbowY, leftElbowZ, leftWristX, leftWristY, leftWristZ, leftHandX, leftHandY, leftHandZ]
                    rightBodyTrackingDict[time] = [rightElbowX, rightElbowY, rightElbowZ, rightWristX, rightWristY, rightWristZ, rightHandX, rightHandY, rightHandZ]
                    
                except:
                    row
        print 'Conversion Complete'
    return leftBodyTrackingDict, rightBodyTrackingDict, pixelFilePath


def prepCircles(convertedFile):
    pixelDict = {}
    header = True
    #print convertedFile
    with open(convertedFile, 'rb') as timePixelPairs:
        for row in timePixelPairs:
            data =  (row.strip()).split(',')
            if header:
                header = False
            else:
                time = (hms_to_seconds(data[0]))
                #draw wrists
                pixelDict[time] = [data[3],data[4],data[9],data[10]]
    tkMessageBox.showinfo("Step 2", "Please select the folder with images within that you wish to paint (eg. RGB)")
    rgbDir = tkFileDialog.askdirectory()
    outputDir = os.path.join(os.path.dirname(rgbDir), 'rgb_drawn')
    imageDict = {}
    rowOne = True
    for rgbCsv in os.listdir(rgbDir):
        if fnmatch.fnmatch(rgbCsv, '*.csv'):
            rgbCsvPath = os.path.join(rgbDir,rgbCsv)
            with open(rgbCsvPath, 'rb') as rgbPairs:
                csvImagePairs = csv.reader(rgbPairs)
                for row in csvImagePairs:
                    if rowOne:
                        rowOne = False #skip header
                    else:
                        time = (hms_to_seconds(row[0]))
                        imageDict[row[1]] = time
    return pixelDict, imageDict, rgbDir, outputDir


def paintCircles(pixelDict, imageDict, rgbDir, outputDir):
    
    #create output dir if does not yet exist
    paintDict = {}
    if not os.path.exists(outputDir):
        os.makedirs(outputDir)
    print 'Preping RGB frames'
    for imageFile in imageDict:
        #print imageFile
        #print imageDict[imageFile]
        closestTime = min(pixelDict, key=lambda x:abs(x-imageDict[imageFile]))
        if abs(closestTime-imageDict[imageFile]) < 1:
            #only draw on image if the image has data less than a second away
            paintDict[os.path.join(rgbDir, imageFile)] = [os.path.join(outputDir, imageFile), pixelDict[closestTime]]
    print 'Drawing converted data onto RGB frames'
    for imgIn in paintDict:
        if imgIn[-4:] == '.jpg':
            #print imgIn
            try:
                inImg = Image.open(imgIn)
                outDir = paintDict[imgIn][0]
                pixels = paintDict[imgIn][1]
                if pixels[0] != '':
                    leftX = int(float(pixels[0]))
                else:
                    leftX = -10
                if pixels[1] != '':
                    leftY = int(float(pixels[1]))
                else:
                    leftY = -10
                if pixels[2] != '':
                    rightX = int(float(pixels[2]))
                else:
                    rightX = -10
                if pixels[3] != '':
                    rightY = int(float(pixels[3]))
                else:
                    rightY = -10
                draw = ImageDraw.Draw(inImg)
                draw.ellipse((leftX-5,leftY-5, leftX+5, leftY+5), fill='green')
                draw.ellipse((rightX-5,rightY-5, rightX+5, rightY+5), fill='red')
                inImg.save(outDir)
            except:
                #print imgIn,'not found - ignore if annotation binned or otherwise'
                imgIn
    print 'Done'
    return paintDict


leftGood = False
rightGood = False
leftMissing = False
rightMissing = False

def y_key(event,root,count):
    global leftGood
    global rightGood
    #print "yes",count
    if count == 0:
        leftGood = True
    elif count == 1:
        rightGood = True
    root.quit()

def n_key(event,root,count):
    #print "no",count
    root.quit()
    
def m_key(event,root,count):
    global leftMissing
    global rightMissing
    #print "missing",count
    if count == 0:
        leftMissing = True
    elif count == 1:
        rightMissing = True
    root.quit()

def humanVerify(root, outputDir, pixelDict, leftBodyTrackingDict, rightBodyTrackingDict, imageDict, rgbDir, paintDict):
    #print 'verify time'
    tkMessageBox.showinfo("Step 3", "Please verify the dots as being on or very near the wrists. Red dots are drawn on the right limb. Images will ask for first left then right verification. Use 'y' key for yes, 'n' key for no, and 'm' key for missing.")
    global leftGood
    global rightGood
    old_label = None
    parentDir = os.path.split(r""+outputDir+"")[0]
    outputTable = os.path.join(parentDir,'wristDataHumanReview.csv')
    if os.path.isfile(outputTable):
        os.remove(outputTable)
    imgFiles = [ f for f in os.listdir(outputDir) if os.path.isfile(os.path.join(outputDir,f)) and not fnmatch.fnmatch(f, '*.csv')]
    
    with open(outputTable, 'wb') as tableOut:
        tableWriter = csv.writer(tableOut)
        header = ['side','img','time','wristBodyX','wristBodyY','wristBodyZ','wristPixelX','wristPixelY','accurate y/n']
        tableWriter.writerow(header)
    
    #randomize order of images in list, optional
    shuffle(imgFiles)
    contQ= True
    imgCount=0
    while contQ:
        for img in imgFiles:
            #print img
            imgTime = imageDict[img]
            paintPixels = paintDict[os.path.join(rgbDir, img)][1]
            #print paintPixels
            leftXPx = paintPixels[0]
            leftYPx = paintPixels[1]
            rightXPx = paintPixels[2]
            rightYPx = paintPixels[3]
            
            closestTime = min(pixelDict, key=lambda x:abs(x-imageDict[img]))
            imgLeftTrackingData = leftBodyTrackingDict[closestTime]
            wristLeftBodyX = imgLeftTrackingData[3]
            wristLeftBodyY = imgLeftTrackingData[4]
            wristLeftBodyZ = imgLeftTrackingData[5]
            
            imgRightTrackingData = rightBodyTrackingDict[closestTime]
            wristRightBodyX = imgRightTrackingData[3]
            wristRightBodyY = imgRightTrackingData[4]
            wristRightBodyZ = imgRightTrackingData[5]
            
            leftGood = False
            rightGood = False
            #print img
            imgPath = os.path.join(outputDir,img)
            #print imgPath
            count = 0
            while count < 2:
                frame = Tkinter.Frame(root)
                image1 = Image.open(imgPath)
                root.geometry('%dx%d' % (image1.size[0],image1.size[1]))
                tkpi = ImageTk.PhotoImage(image1)
                label_image = Tkinter.Label(root, image=tkpi)
                label_image.place(x=0,y=0,width=image1.size[0],height=image1.size[1])
                if count == 0:
                    root.title(img+' left')
                elif count == 1:
                    root.title(img+' right')
                frame.bind("<y>",lambda event: y_key(event,root,count))
                frame.bind("<Y>",lambda event: y_key(event,root,count))
                frame.bind("<n>",lambda event: n_key(event,root,count))
                frame.bind("<N>",lambda event: n_key(event,root,count))
                frame.bind("<m>",lambda event: m_key(event,root,count))
                frame.bind("<M>",lambda event: m_key(event,root,count))
                frame.pack()
                frame.focus_force()
                if old_label is not None:
                    old_label.destroy()
                old_label = label_image
                root.mainloop()
                #print leftGood, rightGood
                count += 1
            with open(outputTable, 'ab') as tableOut:
                tableWriter = csv.writer(tableOut)
                header = ['side','img','time','wristBodyX','wristBodyY','wristBodyZ','wristPixelX','wristPixelY','accurate y/n']
                if leftGood and rightGood:
                    leftData = ['left wrist',img,imgTime,wristLeftBodyX,wristLeftBodyY,wristLeftBodyZ,leftXPx,leftYPx,'y']
                    rightData = ['right wrist',img,imgTime,wristRightBodyX,wristRightBodyY,wristRightBodyZ,rightXPx,rightYPx,'y']
                    #print 'both good'
                elif leftGood:
                    leftData = ['left wrist',img,imgTime,wristLeftBodyX,wristLeftBodyY,wristLeftBodyZ,leftXPx,leftYPx,'y']
                    if rightMissing:
                        rightData = ['right wrist',img,imgTime,wristRightBodyX,wristRightBodyY,wristRightBodyZ,rightXPx,rightYPx,'missing']
                    else:
                        rightData = ['right wrist',img,imgTime,wristRightBodyX,wristRightBodyY,wristRightBodyZ,rightXPx,rightYPx,'n']
                    #print 'left good'
                elif rightGood:
                    if leftMissing:
                        leftData = ['left wrist',img,imgTime,wristLeftBodyX,wristLeftBodyY,wristLeftBodyZ,leftXPx,leftYPx,'missing']
                    else:
                        leftData = ['left wrist',img,imgTime,wristLeftBodyX,wristLeftBodyY,wristLeftBodyZ,leftXPx,leftYPx,'n']
                    rightData = ['right wrist',img,imgTime,wristRightBodyX,wristRightBodyY,wristRightBodyZ,rightXPx,rightYPx,'y']
                    #print 'right good'
                else:
                    if leftMissing:
                        leftData = ['left wrist',img,imgTime,wristLeftBodyX,wristLeftBodyY,wristLeftBodyZ,leftXPx,leftYPx,'missing']
                    else:
                        leftData = ['left wrist',img,imgTime,wristLeftBodyX,wristLeftBodyY,wristLeftBodyZ,leftXPx,leftYPx,'n']
                    if rightMissing:
                        rightData = ['right wrist',img,imgTime,wristRightBodyX,wristRightBodyY,wristRightBodyZ,rightXPx,rightYPx,'missing']
                    else:
                        rightData = ['right wrist',img,imgTime,wristRightBodyX,wristRightBodyY,wristRightBodyZ,rightXPx,rightYPx,'n']
                    #print 'neither good'
                tableWriter.writerow(leftData)
                tableWriter.writerow(rightData)
            
            if imgCount < 150:
                #print el,imgCount
                imgCount += 1
            if imgCount >= 150:
                answer = tkMessageBox.askquestion('Continue?','150 images processed. Continue?')
                #print answer
                if answer == "no":
                    contQ = False
                    exit()
                imgCount = 0


def main():
    root = Tkinter.Tk()
    tkMessageBox.showinfo("Step 1", "Please select the bodyTracking file you want to convert to pixels")
    inputCsv = tkFileDialog.askopenfilename()
    leftBodyTrackingDict, rightBodyTrackingDict, toPixelFile = convert2Pixel(inputCsv)
    pixelDict, imageDict, rgbDir, outputDir = prepCircles(toPixelFile)
    paintDict = paintCircles(pixelDict, imageDict, rgbDir, outputDir)
    humanVerify(root, outputDir, pixelDict, leftBodyTrackingDict, rightBodyTrackingDict, imageDict, rgbDir, paintDict)

main()
